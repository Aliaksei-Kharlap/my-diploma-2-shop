from django.shortcuts import render
from myshop.models import Category, Product, Photo
from cart.forms import CartAddProductForm

def get_main(request):
    categories = Category.objects.all()
    new_products = Product.objects.all().order_by("-time_add")[:5]
    return render(request, 'shophtml/main.html',
                  {'categories':categories, "new_products":new_products})

def get_category(request, pk):
    category = Category.objects.get(pk=pk)
    products = category.product_set.all()
    categories = Category.objects.all()
    return render(request, 'shophtml/category.html',
                  {'category': category, "products": products, "categories":categories})

def get_product(request, pk):
    product = Product.objects.get(pk=pk)
    categories = Category.objects.all()
    cart_product_form = CartAddProductForm()
    return render(request, 'shophtml/product.html',
                  {"product": product, "categories": categories, 'cart_product_form': cart_product_form})
