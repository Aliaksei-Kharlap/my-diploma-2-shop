from django.urls import path
from myshop.views import get_main, get_category, get_product

urlpatterns = [
    path('', get_main, name='main'),
    path('category/<int:pk>/', get_category, name='category_list'),
    path('product/<int:pk>/', get_product, name='product_detail'),
    # path('<int:id>/<slug:slug>/', views.product_detail, name='product_detail'),
]