from django.contrib import admin
from myshop.models import Product, Category, Photo
from django.utils.safestring import mark_safe

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name", "slug"]
    prepopulated_fields = {'slug': ('name',)}



@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["name", "slug", "description", "price", "available"]
    list_editable = ["price", "available"]
    list_filter = ["available"]


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    list_display = ["name", "image", "product"]

